<?php
require_once "DBHandler.php";

if(!isset($_POST['action']))
{ 
	die('Undefined action');
}

session_start();

switch($_POST['action']) {
	
	case 'isUserLoggedIn':
		
		echo (isUserLoggedIn()) ? 1 : 0;
		break;
	
	case 'logIn':
	
		echo (logIn()) ? 1 : 0;
		break;
	
	case 'logOut':
		
		logOut();
		break;
		
	case 'postMessage':
		
		postMessage();
		break;
	
	case 'getMessages':
		
		getMessages();	
		break;
		
	default:		
		die('Undefined action');
}

die();

function logIn() 
{
	if(!isset($_POST['userName']) || !isset($_POST['password'])) {
	
		return false;
	}

	$userName = $_POST['userName'];
	$password = $_POST['password'];

	$DBH 	= new DBHandler();
	$result = $DBH->query("SELECT * FROM SB_USER WHERE USER_NAME = '$userName' AND PASSWORD = '$password'");
	
	if(!empty($result)) {
		
		$_SESSION['user_login_status'] = 1;
		$_SESSION['user_id'] = $result[0]['ID'];
		
		return true;
	}
	
	return false;
}

function logOut()
{
	$_SESSION = array();
	session_destroy();
}

function isUserLoggedIn()
{
	if(isset($_SESSION['user_login_status']) && $_SESSION['user_login_status'] === 1) {
	
		return true;
	}	
	return false;
}

function postMessage()
{
	if(!isUserLoggedIn() || !isset($_POST['messageContent'])) {
		die('User not logged or message content not defined');
	}

	$DBH = new DBHandler();
	
	$messageContent = mysql_escape_string($_POST['messageContent']);
	$postDate 		= date('Y-m-d H:i:s');
	$userId 		= $_SESSION['user_id'];
	
	$result = $DBH->query("INSERT INTO SB_MESSAGE (USER_ID, CREATED, CONTENT) VALUES ($userId, '$postDate', '$messageContent')");
}

function getMessages()
{
	if(!isUserLoggedIn()) {
	
		die('User not logged or message content not defined');
	}

	$DBH = new DBHandler();
	$result = $DBH->query("SELECT SB_MESSAGE.*, SB_USER.USER_NAME FROM SB_MESSAGE, SB_USER WHERE SB_MESSAGE.USER_ID = SB_USER.ID ORDER BY SB_MESSAGE.CREATED ASC");
	echo json_encode($result);
}



