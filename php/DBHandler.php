<?php

/*
* DBHandler
*	
*/
class DBHandler
{
	private $pdo;
	
	private $dbHost		= "";
	private $dbName 	= "";
	private $dbUser 	= "";
	private $dbPassword = "";
	
	public function __construct()
	{
		try {
		
			// TODO: Database info should be stored into some config-file.
			$this->pdo = new PDO('mysql:host=$dbHost;dbname=$dbName', '$dbUser', '$dbPassword');
			
		} catch (PDOException $e) {
		
			echo "Database connection failed: " . $e->getMessage() . "\n";
			exit;
		}			
	}
	
	/*
	* select
	*
	* Prepares and excutes SQL select-statement with given parameters
	* using PDO-class. Returns query result as an array.
	*
	* TODO: Implement usage of the fields- and conds-parameters!
	*
	* @param	String	$table
	* @param	Mixed	$fields
	* @param	Mixed	$conds
	* @param	Mixed	$params
	*
	* @return	Array	$data	Query result data
	*	
	*/
	public function select($table = null, $fields = null, $conds = null, $params = null)
	{		
		// Table-name must be provovided as a string
		if (is_null($table) || !is_string($table)) {
			return false;
		}
		
		if (is_null($fields)) {
			$fields = "*";
		}
		
		$statement = "SELECT $fields FROM $table WHERE 1 $params";
		$result = $this->pdo->query($statement);	
		$data = $result->fetchAll(PDO::FETCH_ASSOC);	
		
		return $data;
	}
	
	public function query($statement) {
	
		$result = $this->pdo->query($statement);
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}
	
}

?>