
-----------------------------------------------------

SB_USER:

CREATE TABLE `SB_USER` (
	`ID` INT(10) NOT NULL AUTO_INCREMENT,
	`USER_NAME` VARCHAR(255) NOT NULL,
	`PASSWORD` VARCHAR(255) NULL DEFAULT NULL,
	PRIMARY KEY (`ID`)
)

-----------------------------------------------------

-----------------------------------------------------

SB_MESSAGE:

CREATE TABLE `SB_MESSAGE` (
	`ID` INT(10) NOT NULL AUTO_INCREMENT,
	`USER_ID` INT(10) NOT NULL,
	`CREATED` DATETIME NOT NULL,
	`CONTENT` TEXT NULL,
	PRIMARY KEY (`ID`)
)

-----------------------------------------------------