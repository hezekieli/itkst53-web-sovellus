
var pollTimer;
var polling = false;

$(document).ready(function() {
	
	if(isUserLoggedIn()) {
		initChatView();
	} else {
		initLogInView();
	}
});

function initHandlers() {
	
	$("#logInForm").submit(function(event){
		
		event.preventDefault();
		logIn(event.target);
	});
}

function isUserLoggedIn() {
	
	var loggedIn = false;
	
	$.ajax(
		{
			type: 'POST',
			url: 'php/shoutbox.php',
			data: "action=isUserLoggedIn",
			async: false,
			success: function(result){
				if(parseInt(result) == 1) {
					loggedIn = true;
				}
			},
			failure: function(msg){
				console.info("Ajax failure: " + msg);
			}
		}
	);
	
	return loggedIn;
}

function initLogInView() {

	$("#content").load("login.html");
}

function initChatView() {
	
	$("#logIn").remove();
	$("#content").load("chat.html");
}

function logIn(form) {
	
	var postData = $(form).serialize();
		
	$.ajax(
		{
			type: 'POST',
			url: 'php/shoutbox.php',
			data: postData + '&action=logIn',
			success: function(result){
				var res = parseInt(result);
				if(res == 0) {
					$("#logInError").css("display", "inline-block");
				} else if(res == 1) {
					initChatView();
				}
			},
			failure: function(msg){
				console.info("Ajax failure: " + msg);
			}
		}
	);
}

function logOut(form) {
	
	clearInterval(pollTimer);
	
	$.ajax(
		{
			type: 'POST',
			url: 'php/shoutbox.php',
			data: 'action=logOut',
			success: function(result){
				initLogInView();
			},
			failure: function(msg){
				console.info("Ajax failure: " + msg);
			}
		}
	);
}

function initMessagePollTimer() {

	pollTimer = setInterval(function(){ updateMessageList(false) }, 1500);
}

function updateMessageList(messages) {

	if(!messages && !polling) {
		
		polling = true;
		getMessages();
		
	} else if(messages) {
		
		$("#messageList").html("");
		
		for(var i = 0; i < messages.length; i++) {
			var message = createMessageElement(messages[i]);
			$("#messageList").append($(message));	
		}
	}
}

function getMessages() {
	
	$.ajax(
		{
			type: 'POST',
			dataType: 'json',
			url: 'php/shoutbox.php',
			data: 'action=getMessages',
			success: function(result){
				polling = false;
				updateMessageList(result);
			},	
			failure: function(msg){
				console.info("Ajax failure: " + msg);
			}
		}
	);

}

function createMessageElement(messageData) {
		
	var message = $('<li class="message"><span class="msgPostTime"></span><span class="msgUserName"></span><br/><span class="msgContent"></span></li>');
	
	var dateTime = messageData.CREATED.split(" ");
	
	$(message).find(".msgPostTime").text(dateTime[1]);
	$(message).find(".msgUserName").text(messageData.USER_NAME);
	$(message).find(".msgContent").text(messageData.CONTENT);
	
	return message;
}

function postMessage() {
	
	var message = $("#messageInput").val();
	$("#messageInput").val("");
	
	$.ajax(
		{
			type: 'POST',
			url: 'php/shoutbox.php',
			data: 'action=postMessage' + "&messageContent=" + message,
			success: function(result){
				//updateMessageList(result);
			},
			failure: function(msg){
				console.info("Ajax failure: " + msg);
			}
		}
	);
	
}



